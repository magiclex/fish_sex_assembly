#!/usr/bin/env nextflow
/* 
======================================================================================== 
SCAF : Sex Chromosom Assembly for Fish, a Nexflow pipeline 
========================================================================================
 
 #### Homepage / Documentation
 #https://gitlab.com/... 
---------------------------------------------------------------------------------------- 
*/ 

nextflow.enable.dsl=2 

def helpMsg() {
    // Add to this help message with new command line parameters
    log.info"""

	Usage:
	(command)
	
	Parameters:
	
Mandatory:
	(parameters)

Other options:
	(parameters)

	""".stripIndent()
}


// Help message:
if (params.help) {
	helpMsg()
	exit 0
}



// SET UP CONFIGURATION VARIABLES 
// Has the run name been specified by the user? 
// this has the bonus effect of catching both -name and --name
custom_runName = params.name 
if (!(workflow.runName ==~ /[a-z]+_[a-z]+/)) {
 custom_runName = workflow.runName
}
//Copy config files to output directory for each run
paramsfile = file("$baseDir/conf/base.config", checkIfExists: true) 
paramsfile.copyTo("$params.outdir/00_pipeline_info/base.config")


// PIPELINE INFO
// Header log info
log.info SCAFHeader()
def summary = [:]
if (workflow.revision) summary['Pipeline Release'] = workflow.revision
summary['Run Name'] = custom_runName ?: workflow.runName
summary['Project Name'] = params.projectName
summary['User'] = workflow.userName
summary['Output dir'] = params.outdir
summary['Launch dir'] = workflow.launchDir
summary['Working dir'] = workflow.workDir
summary['Script dir'] = workflow.projectDir
//summary['Illumina file(s)'] = params.illumina_path
//summary['PacBio file(s)'] = params.pacbio_path
// ^^^ add the options files
log.info summary.collect { k,v -> "${k.padRight(18)}: $v" }.join("\n")
log.info "-\033[91m--------------------------------------------------\033[0m-"

// Check hostnames against configured profiles
checkHostname()


/*
 * IMPORTING MODULES
 */

//include {nm_module} from './modules/nm_module.nf'

include { JELLYFISH_COUNT } from './modules/local/jellyfish/count/main.nf'

include { JELLYFISH_HISTO } from './modules/local/jellyfish/histo/main.nf'

include { GENOMESCOPE } from './modules/local/genomescope/main.nf'

include { FILEOFFILE } from './modules/local/FileOfFile/main.nf'

include { KMTRICKS } from './modules/local/kmtricks/main.nf'

include { SELECT_KMERS } from './modules/local/selectKmers/main.nf'

include { HIFIASM } from './modules/local/hifiasm/main.nf'

include { BACK_TO_SEQUENCES } from './modules/local/backToSequences/first/main.nf'

include { BWAMEM } from './modules/local/bwamem2/main.nf'

include { FIRST_SAMTOOLS } from './modules/local/samtools/first/main.nf'

include { FIRST_BEDTOOLS } from './modules/local/bedtools/first/main.nf'

include { SECOND_BEDTOOLS } from './modules/local/bedtools/second/main.nf'

include { SLID_WIND } from'./modules/local/slidwind/main.nf'

include { GET_CONTIGS } from './modules/local/getcontigs/main.nf'

include { MINIMAP } from './modules/local/minimap2/main.nf'

include { SECOND_SAMTOOLS } from './modules/local/samtools/second/main.nf'

include { YAHS } from './modules/local/yahs/main.nf'

include { DRAW_SCAF } from './modules/local/draw_scaf/main.nf'

include { SECOND_BTS } from './modules/local/backToSequences/second/main.nf'

include { DRAW_CURVE } from './modules/local/draw_curve/main.nf'


/*
 * CREATE INPUT CHANNELS
 */
pool_male_r1_ch = Channel.fromPath("${params.pool_m_r1_path}")
pool_male_r2_ch = Channel.fromPath("${params.pool_m_r2_path}")
pool_female_r1_ch = Channel.fromPath("${params.pool_f_r1_path}")
pool_female_r2_ch = Channel.fromPath("${params.pool_f_r2_path}")
pacbio1_ch = Channel.fromPath("${params.pacbio1_path}") 
pacbio2_ch = Channel.fromPath("${params.pacbio2_path}") 

hic_r1_ch = Channel.fromPath("${params.hic_r1_path}")
hic_r2_ch = Channel.fromPath("${params.hic_r2_path}")

//fof_ch = Channel.fromPath("${params.fof_path}") //
species_ch = Channel.of("${params.species}")

// Normally shouldn't be necessary in DSL2, only one channel should work
species2_ch = Channel.of("${params.species}")
species3_ch = Channel.of("${params.species}")
species4_ch = Channel.of("${params.species}")

threshold_1_ch = Channel.of("${params.threshold_1}")
threshold_2_ch = Channel.of("${params.threshold_2}")

//need to change that; tuple with values and suffix (in a file?)
suf_m=Channel.of("male")

size_k_ch=Channel.of("${params.kmer_size}")

obj_ch=Channel.of("${params.object_type}")

bin_size_ch=Channel.of("${params.bins_size}")

draw_size_ch=Channel.of("${params.draw_size}")

tab_male_file = Channel.of("${params.pool_m_r1_path}", "${params.pool_m_r2_path}").collect()

tab_female_file = Channel.of("${params.pool_f_r1_path}", "${params.pool_f_r2_path}").collect()

plugin_ch = Channel.fromPath("${params.plugin}")

percentage_ch = Channel.of("${params.percentage}")

scriptdir_ch = Channel.of("${params.scriptdir}")

enzymes_ch = Channel.of("${params.enzymes}")

/*
 * RUN MAIN WORKFLOW
 */
workflow {
	//description processus
	// processus(input)

    // 00_01_jellyfish_count
    JELLYFISH_COUNT(pool_male_r1_ch,pool_male_r2_ch,suf_m)
    // DONE

    // 00_02_jellyfish_histo
    JELLYFISH_HISTO(JELLYFISH_COUNT.out.count_tuple)
    // DONE

    // 00_03_genomescope
    //GENOMESCOPE(JELLYFISH_HISTO.out.histo_tuple,size_k_ch)
    // NOT WORKING

    // 01_01_fileoffile
    FILEOFFILE(tab_male_file,tab_female_file,species_ch)
    // DONE

    // 01_02_kmtricks find the kmers in each pool and return the exclusive ones with their abundance in each pool
    KMTRICKS(plugin_ch,FILEOFFILE.out.fof_ch,species2_ch)
    // DONE

    // 02_01_select_kmers separate the kmers into separate files, if their abundance is over the threshold
    SELECT_KMERS(species3_ch,KMTRICKS.out.kmtricks_ch,threshold_1_ch)
    // DONE

    unclean_kmer_both_ch = SELECT_KMERS.out.unclean_male_kmer_fasta_ch.concat(SELECT_KMERS.out.unclean_female_kmer_fasta_ch)
    // DONE

    // 03_01_hifiasm assembly of the longs reads into two sets of contigs
    //HIFIASM(species4_ch,hic_r1_ch,hic_r2_ch,pacbio1_ch,pacbio2_ch)
    // NOT TESTED

    //cross_ch = SELECT_KMERS.out.unclean_kmer_both_ch.combine(HIFIASM.out.hapall_ch)

    // 04_01_back_to_sequences selections of the contigs with specific kmers
    //BACK_TO_SEQUENCES(cross_ch,threshold_2_ch,kmer_size)
    // NOT TESTED

    //contig_kmer_ch=BACK_TO_SEQUENCES.out.btos_contig_ch.combine(BACK_TO_SEQUENCES.out.btos_kmers_ch, by: 0)

    // 05_01_bwamem2 
    //BWAMEM(contig_kmer_ch,kmer_size)
    // NOT TESTED 

    // 05_02_samtools
    //FIRST_SAMTOOLS(BWAMEM.out.bwamem_sam_ch)
    // NOT TESTED (check if index not required)
    
    // 05_03_first_bedtools
    //FIRST_BEDTOOLS(FIRST_SAMTOOLS.out.first_samtools_sorted_ch)
    // NOT TESTED

    //bedtools_n_genome_ch=FIRST_BEDTOOLS.out.first_bedtools_ch.combine(FIRST_SAMTOOLS.out.first_samtools_genome_ch, by:0)

    // 06_01_second_bedtools
    //SECOND_BEDTOOLS(bedtools_n_genome_ch)
    // NOT TESTED

    // 06_02_Rscript_slid_wind_pipeline
    //SLID_WIND(SECOND_BEDTOOLS.out.second_bedtools_ch,obj_ch,bin_size_ch,threshold_2_ch,scriptdir_ch)
    // NOT TESTED

    // 07_01_get_contigs_python3
    //GET_CONTIGS(SLID_WIND.out.slid_uniq_contig_ch,BACK_TO_SEQUENCES.out.btos_contig_ch,species_ch,scriptdir_ch)
    // NOT TESTED

    // 07_02_minimap2
    //MINIMAP(hic_r1_ch,hic_r2_ch,GET_CONTIGS.out.get_contig_ch,species_ch)
    // NOT TESTED

    // NEED TO LINK CHANNELS USING THE SUFFIX

    // 07_03_second_samtools
    //SECOND_SAMTOOLS(MINIMAP.out.minimap2_sam_ch,GET_CONTIGS.out.get_contig_ch,species_ch)

    // 07_04_yahs
    //YAHS(SECOND_SAMTOOLS.out.second_samtools_fai_ch,SECOND_SAMTOOLS.out.second_samtools_fa_ch,SECOND_SAMTOOLS.out.second_samtools_sorted_ch,species_ch,enzymes_ch)

    // 08_01_draw_scaf
    //DRAW_SCAF(YAHS.out.yahs_agp_ch,SLID_WIND.out.slid_uniq_tsv_ch,bin_size_ch,scriptdir_ch)
    // NEED CHANGES IN THE SCRIPT TO GET THE SUFFIX IN THE NAME

    //kmer_n_scaf_ch=unclean_kmer_both_ch.combine(YAHS.out.yahs_scaffold_ch, by: 0)
    // 09_01_second_back_to_sequences
    //SECOND_BTS(kmer_n_scaf_ch,species_ch,kmer_size)
    // NOT TESTED

    // 09_02_draw_curve
    //DRAW_CURVE(YAHS.out.yahs_agp_ch,draw_size_ch,scriptdir_ch)
    //NEED CHANGES TO AGGREGATE BY SUFFIX
}

/*
 * Completion notification
 */
workflow.onComplete {
    c_green = params.monochrome_logs ? '' : "\033[0;32m";
    c_purple = params.monochrome_logs ? '' : "\033[0;35m";
    c_red = params.monochrome_logs ? '' : "\033[0;31m";
    c_reset = params.monochrome_logs ? '' : "\033[0m";
    
    def msg = """\
        Pipeline execution summary
        ---------------------------
        Completed at: ${workflow.complete}
        Duration    : ${workflow.duration}
        Success     : ${workflow.success}
        workDir     : ${workflow.workDir}
        exit status : ${workflow.exitStatus}
        """
    .stripIndent()
    if (workflow.success) {
        log.info "-${c_purple}[Workflow info]${c_green} workflow completed successfully${c_reset}-"
    } else {
        checkHostname()
        log.info "-${c_purple}[Workflow info]${c_red} workflow completed with errors${c_reset}-"
    }
}

/*
 * Other functions
 *
 */
def SCAFHeader() {
    // Log colors ANSI codes
    c_red = params.monochrome_logs ? '' : "\033[0;91m";
    c_blue = params.monochrome_logs ? '' : "\033[1;94m";
    c_reset = params.monochrome_logs ? '' : "\033[0m";
    c_yellow = params.monochrome_logs ? '' : "\033[1;93m";
    c_Ipurple = params.monochrome_logs ? '' : "\033[0;95m" ;

    return """    -${c_red}--------------------------------------------------${c_reset}-


    ${c_blue}  _____ _____ _____ _____   ${c_blue}
    ${c_blue} |   __|     |  _  |   __|  ${c_blue} 
    ${c_blue} |__   |   --|     |   __|  ${c_blue}   
    ${c_blue} |_____|_____|__|__|__|     ${c_blue}

    ${c_yellow}  Sexual Chromosom Assembly for Fish (version ${workflow.manifest.version})${c_reset}
                                            ${c_reset}
    ${c_Ipurple}  Homepage: ${workflow.manifest.homePage}${c_reset}
    -${c_red}--------------------------------------------------${c_reset}-
    """.stripIndent()
}
 

def checkHostname() {
    def c_reset = params.monochrome_logs ? '' : "\033[0m"
    def c_white = params.monochrome_logs ? '' : "\033[0;37m"
    def c_red = params.monochrome_logs ? '' : "\033[1;91m"
    def c_yellow_bold = params.monochrome_logs ? '' : "\033[1;93m"
    if (params.hostnames) {
        def hostname = "hostname".execute().text.trim()
        params.hostnames.each { prof, hnames ->
            hnames.each { hname ->
                if (hostname.contains(hname) && !workflow.profile.contains(prof)) {
                    log.error "====================================================\n" +
                            "  ${c_red}WARNING!${c_reset} You are running with `-profile $workflow.profile`\n" +
                            "  but your machine hostname is ${c_white}'$hostname'${c_reset}\n" +
                            "  ${c_yellow_bold}It's highly recommended that you use `-profile $prof${c_reset}`\n" +
                            "============================================================"
                }
            }
        }
    }
}

