#!/usr/bin/env nextflow

/*
 * Select the kmers specific to each group
 */

process SELECT_KMERS{
	label 'midmem'

        tag "SELECT_KMERS"

        publishDir "${params.resultdir}/select_kmers",   mode: 'copy', pattern: '*.txt'
        publishDir "${params.resultdir}/select_kmers",   mode: 'copy', pattern: '*.fasta'
        publishDir "${params.resultdir}/logs/select_kmers",      mode: 'copy', pattern: 'select_kmers*.log'
        publishDir "${params.outdir}/00_pipeline_info/cmd",  mode: 'copy', pattern: 'select_kmers*.cmd'

        input:
                val(species)
                path(specific_kmers)
		val(limit)

        output:
		tuple val("male"), path("*_male.fasta"), emit: unclean_male_kmer_fasta_ch
                tuple val("female"), path("*_female.fasta"), emit: unclean_female_kmer_fasta_ch
                path("select_kmers*.log")
                path("select_kmers*.cmd")

        script:
        """
        select_kmers.sh ${species} ${specific_kmers} ${limit} ${task.cpus} select_kmers.cmd >& select_kmers.log 2>&1
        """

}
