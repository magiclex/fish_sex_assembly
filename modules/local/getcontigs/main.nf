#!/usr/bin/env nextflow

/*
 * Check the content of the windows with R
 */

process GET_CONTIGS{
	label 'midmem'

	tag "GET_CONTIGS"

	publishDir "${params.resultdir}/get_contigs",	mode: 'copy', pattern: '*.txt'
	publishDir "${params.resultdir}/get_contigs",	mode: 'copy', pattern: '*.tsv'
	publishDir "${params.resultdir}/logs/get_contigs",	mode: 'copy', pattern: 'get_contigs*.log'
	publishDir "${params.outdir}/00_pipeline_info/cmd",  mode: 'copy', pattern: 'get_contigs*.cmd'

	input:
        tuple val(suffix), path(selected)
        path(hapall)
        val(species)
        //may be changed
        path(scriptdir)


	output:
		tuple val(suffix), path("*.fa"), emit: get_contig_ch
		path("get_contigs*.log")
		path("get_contigs*.cmd")

	script:
	"""
	get_contigs.sh ${selected} ${hapall} ${species} ${suffix} ${scriptdir} get_contigs.cmd >& get_contigs.log 2>&1
	"""
}