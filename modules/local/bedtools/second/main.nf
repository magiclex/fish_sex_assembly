#!/usr/bin/env nextflow

/*
 * Create windows for the coverage
 */

process SECOND_BEDTOOLS{
	label 'midmem'

	tag "SECOND_BEDTOOLS"

	publishDir "${params.resultdir}/second_bedtools",	mode: 'copy', pattern: '*_coveragePerBin.bed'
	publishDir "${params.resultdir}/logs/second_bedtools",	mode: 'copy', pattern: 'second_bedtools*.log'
	publishDir "${params.outdir}/00_pipeline_info/cmd",  mode: 'copy', pattern: 'second_bedtools*.cmd'

	input:
        tuple val(suffix), path(bed), path(genome)

	output:
		tuple val(suffix), path("*_coveragePerBin.bed"), emit: second_bedtools_ch
		path("second_bedtools*.log")
		path("second_bedtools*.cmd")

	script:
	"""
	second_bedtools.sh ${bed} ${genome} second_bedtools.cmd >& second_bedtools.log 2>&1
	"""
}