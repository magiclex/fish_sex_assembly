#!/usr/bin/env nextflow

/*
 * Transform a bam file to a bed file
 */

process FIRST_BEDTOOLS{
	label 'midmem'

	tag "FIRST_BEDTOOLS"

	publishDir "${params.resultdir}/first_bedtools",	mode: 'copy', pattern: '*.bed'
	publishDir "${params.resultdir}/logs/first_bedtools",	mode: 'copy', pattern: 'first_bedtools*.log'
	publishDir "${params.outdir}/00_pipeline_info/cmd",  mode: 'copy', pattern: 'first_bedtools*.cmd'

	input:
        tuple val(suffix), path(bam)

	output:
		tuple val(suffix), path("*.bed"), emit: first_bedtools_ch
		path("first_bedtools*.log")
		path("first_bedtools*.cmd")

	script:
	"""
	first_bedtools.sh ${bam} first_bedtools.cmd >& first_bedtools.log 2>&1
	"""
}