#!/usr/bin/env nextflow

/*
 * Index the contigs and map the kmers on them
 */

process BWAMEM{

    label 'lowmem'

    tag 'BWAMEM'

    publishDir "${params.resultdir}/bwamem", mode: 'copy', pattern: '*.sam'
    publishDir "${params.resultdir}/logs/bwamem",	mode: 'copy', pattern: 'bwamem*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'bwamem*.cmd'


    input:
        tuple val(suffix), path(hapall), path(kmers)
        val(ksize)


    output:
        tuple val(suffix), path("*.sam"), emit: bwamem_sam_ch
        path("bwamem*.log")
		path("bwamem*.cmd")

    script:
    """
    bwamem.sh ${hapall} ${kmers} ${ksize} ${suffix} bwamem.cmd >& bwamem.log 2>&1

    """

}