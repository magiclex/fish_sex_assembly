#!/usr/bin/env nextflow

/*
 * Get the position of the kmers on the scaffolds
 */

process SECOND_BTS{

    label 'lowmem'

    tag 'SECOND_BTS'

    publishDir "${params.resultdir}/secondBtoS", mode: 'copy', pattern: '*.fasta'
    publishDir "${params.resultdir}/secondBtoS", mode: 'copy', pattern: '*.txt'
    publishDir "${params.resultdir}/logs/secondBtoS",	mode: 'copy', pattern: 'secondBtoS*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'secondBtoS*.cmd'


    input:
        tuple val(suffix), path(kmers), path(scaffold)
        val(species)
        val(ksize)


    output:
        tuple val(suffix), path("*_scaffolds_kmers_*.fasta"), emit: sec_btos_fasta_ch
        path("*.txt")
        path("secondBtoS*.log")
		path("secondBtoS*.cmd")

    script:
    """
    second_back_to_sequences.sh ${scaffold} ${kmers} ${species} ${suffix} ${ksize} secondBtoS.cmd >& secondBtoS.log 2>&1

    """

}