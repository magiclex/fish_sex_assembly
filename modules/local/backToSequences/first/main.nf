#!/usr/bin/env nextflow

/*
 * Select the contigs containing the sex-specific kmers
 */

process BACK_TO_SEQUENCES{

    label 'lowmem'

    tag 'BACK_TO_SEQUENCES'

    publishDir "${params.resultdir}/backToSequences", mode: 'copy', pattern: '*.fasta'
    publishDir "${params.resultdir}/backToSequences", mode: 'copy', pattern: '*.txt'
    publishDir "${params.resultdir}/logs/backToSequences",	mode: 'copy', pattern: 'backToSequences*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'backToSequences*.cmd'


    input:
        tuple val(suffix), path(kmers), path(hapall)
        val(threshold)
        val(ksize)

    output:
        tuple val(suffix), path("contig*.fasta"), emit: btos_contig_ch
        tuple val(suffix), path("name_contigs*.txt"), emit: btos_names_ch
        tuple val(suffix), path("km_no_null*.fasta"), emit: btos_kmers_ch
        path("backToSequences*.log")
		path("backToSequences*.cmd")

    script:
    """
    back_to_sequences.sh ${suffix} ${hapall} ${kmers} ${threshold} ${ksize} backToSequences.cmd >& backToSequences.log 2>&1

    """

}