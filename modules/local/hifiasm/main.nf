#!/usr/bin/env nextflow

/*
 * Make an assembly of long reads with the help of Hi-C reads
 */

process HIFIASM{

    label 'midmem'

    tag 'HIFIASM'

    publishDir "${params.resultdir}/hifiasm", mode: 'copy', pattern: '*hapall.fa'
    publishDir "${params.resultdir}/logs/hifiasm",	mode: 'copy', pattern: 'hifiasm*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'hifiasm*.cmd'


    input:
        val(species)
        path(hic1)
        path(hic2)
        path(pacbio1)
        path(pacbio2)


    output:
        //tuple val("hap1"), path ("*hap1*.fa"), emit: hap1_ch
        //tuple val("hap2"), path ("*hap2*.fa"), emit: hap2_ch
        path("*hapall.fa"), emit: hapall_ch
        path("hifiasm*.log")
		path("hifiasm*.cmd")

    script:
    """
    #module load bioinfo/hifiasm/0.19.5
    hifiasm.sh ${species} ${hic1} ${hic2} ${pacbio1} ${pacbio2} hifiasm.cmd >& hifiasm.log 2>&1

    """

}