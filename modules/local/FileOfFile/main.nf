#!/usr/bin/env nextflow

/*
 * Create a File of File
 */

process FILEOFFILE{

    label 'lowmem'

    tag 'FILEOFFILE'

    publishDir "${params.resultdir}/fileoffile", mode: 'copy'
    publishDir "${params.resultdir}/logs/fileoffile",	mode: 'copy', pattern: 'fileoffile*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'fileoffile*.cmd'


    input:
        val tab_male_file
        val tab_female_file
        val species

    output:
        path("*.txt"), emit: fof_ch
		path("fileoffile*.log")
		path("fileoffile*.cmd")
        

    script:
    """
    echo ${tab_male_file}
    echo ${tab_female_file}
    male_files="\$(echo '${tab_male_file.join(" ")}')"
    female_files="\$(echo '${tab_female_file.join(" ")}')"

    #fileoffile.sh ${tab_male_file} ${tab_female_file} ${species} fileoffile.cmd >& fileoffile.log 2>&1
    fileoffile.sh "\${male_files}" "\${female_files}" ${species} fileoffile.cmd >& fileoffile.log 2>&1


    """

}