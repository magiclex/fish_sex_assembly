#!/usr/bin/env nextflow

/*
 * Transform a sam file to a bam file, remove the unmapped, create an index
 */

process SECOND_SAMTOOLS{
	label 'midmem'

	tag "SECOND_SAMTOOLS"

	publishDir "${params.resultdir}/second_samtools",	mode: 'copy', pattern: '*.bam'
    publishDir "${params.resultdir}/second_samtools",	mode: 'copy', pattern: '*.fai'
	publishDir "${params.resultdir}/logs/second_samtools",	mode: 'copy', pattern: 'second_samtools*.log'
	publishDir "${params.outdir}/00_pipeline_info/cmd",  mode: 'copy', pattern: 'second_samtools*.cmd'

	input:
        tuple val(suffix), path(sam)
        tuple val(suffix2), path(selected_cont)
		//suffix and suffix2 should be the same
        val(species)

	output:
		tuple val(suffix), path("*.bam"), emit: second_samtools_bam_ch
        tuple val(suffix), path("*_sorted.bam"), emit: second_samtools_sorted_ch
        tuple val(suffix), path(selected_cont), emit : second_samtools_fa_ch // selected contig from the input, may not be needed
        tuple val(suffix), path("*.fai"), emit: second_samtools_fai_ch
		path("second_samtools*.log")
		path("second_samtools*.cmd")

	script:
	"""
	second_samtools.sh ${sam} ${selected_cont} ${species} ${suffix} second_samtools.cmd >& second_samtools.log 2>&1
	"""
}