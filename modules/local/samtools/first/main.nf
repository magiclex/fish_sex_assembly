#!/usr/bin/env nextflow

/*
 * Transform a sam file to a bam file, sort it, get the genome and the coverage
 */

process FIRST_SAMTOOLS{
	label 'midmem'

	tag "FIRST_SAMTOOLS"

	publishDir "${params.resultdir}/first_samtools",	mode: 'copy', pattern: '*.bam'
    publishDir "${params.resultdir}/first_samtools",	mode: 'copy', pattern: '*.tsv'
    publishDir "${params.resultdir}/first_samtools",	mode: 'copy', pattern: '*.txt'
	publishDir "${params.resultdir}/logs/first_samtools",	mode: 'copy', pattern: 'first_samtools*.log'
	publishDir "${params.outdir}/00_pipeline_info/cmd",  mode: 'copy', pattern: 'first_samtools*.cmd'

	input:
        tuple val(suffix), path(sam)

	output:
		tuple val(suffix), path("*male.bam"), emit: first_samtools_bam_ch
        tuple val(suffix), path("*_sorted.bam"), emit: first_samtools_sorted_ch
        tuple val(suffix), path("*.tsv"), emit: first_samtools_coverage_ch
        tuple val(suffix), path("*.txt"), emit: first_samtools_genome_ch
		path("first_samtools*.log")
		path("first_samtools*.cmd")

	script:
	"""
	first_samtools.sh ${sam} first_samtools.cmd >& first_samtools.log 2>&1
	"""
}
