#!/usr/bin/env nextflow

/*
 * Draw the curves.
 */

process DRAW_CURVE{
	label 'midmem'

	tag "DRAW_CURVE"

	publishDir "${params.resultdir}/draw_curve",	mode: 'copy', pattern: '*.png'
	publishDir "${params.resultdir}/logs/draw_curve",	mode: 'copy', pattern: 'draw_curve*.log'
	publishDir "${params.outdir}/00_pipeline_info/cmd",  mode: 'copy', pattern: 'draw_curve*.cmd'

	input:
        tuple val(suffix), path(kmers)
        tuple val(suffix2), path(agpfile)
		//suffix and suffix2 should be the same
        val(drawbin)
        path(scriptdir)


	output:
		path("*.png")   //need to verify this
		path("draw_curve*.log")
		path("draw_curve*.cmd")

	script:
	"""
	draw_curve.sh ${kmers} ${agpfile} ${drawbin} ${scriptdir} draw_curve.cmd >& draw_curve.log 2>&1
	"""
}