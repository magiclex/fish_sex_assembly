#!/usr/bin/env nextflow

/*
 * Count kmers in a fasta file
 */

process JELLYFISH_COUNT{

    label 'lowmem'

    tag 'JELLYFISH_COUNT'

    publishDir "${params.resultdir}/jellyfish_count",	mode: 'copy', pattern: 'reads*.jf'
    publishDir "${params.resultdir}/logs/jellyfish_count",	mode: 'copy', pattern: 'jellyfish_count*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'jellyfish_count*.cmd'


    input:
        path(fasta_1)
        path(fasta_2)
        val(suffix)

    output:
        tuple path("reads*.jf"), val(suffix), emit: count_tuple	
		path("jellyfish_count*.log")
		path("jellyfish_count*.cmd")

    script:
    """

    jellyfish_count.sh ${fasta_1} ${fasta_2} ${suffix} ${task.cpus} jellyfish_count.cmd >& jellyfish_count.log 2>&1

    """
}