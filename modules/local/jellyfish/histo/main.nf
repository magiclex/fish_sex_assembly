#!/usr/bin/env nextflow

/*
 * Make an histogram of a count of kmers
 */

process JELLYFISH_HISTO{

    label 'lowmem'

    tag 'JELLYFISH_HISTO'

    publishDir "${params.resultdir}/jellyfish_histo",	mode: 'copy', pattern: '*.histo'
    publishDir "${params.resultdir}/logs/jellyfish_histo",	mode: 'copy', pattern: 'jellyfish_histo*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'jellyfish_histo*.cmd'

    input:
        tuple( path(count), val(suffix) )
        

    output:
        tuple path("*.histo"), val(suffix), emit: histo_tuple	
		path("jellyfish_histo*.log")
		path("jellyfish_histo*.cmd")

    script:
    """
    #module load bioinfo/Jellyfish/2.3.0
    jellyfish_histo.sh ${count} ${suffix} ${task.cpus} jellyfish_histo.cmd >& jellyfish_histo.log 2>&1
    """
}