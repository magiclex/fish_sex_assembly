#!/usr/bin/env nextflow

/*
 * Check the content of the windows with R
 */

process SLID_WIND{
	label 'midmem'

	tag "SLID_WIND"

	publishDir "${params.resultdir}/slid_wind",	mode: 'copy', pattern: '*.txt'
	publishDir "${params.resultdir}/slid_wind",	mode: 'copy', pattern: '*.tsv'
	publishDir "${params.resultdir}/logs/slid_wind",	mode: 'copy', pattern: 'slid_wind*.log'
	publishDir "${params.outdir}/00_pipeline_info/cmd",  mode: 'copy', pattern: 'slid_wind*.cmd'

	input:
        tuple val(suffix), path(coveragePerBin)
        val(obj)
        val(bin)
        val(percent)
        //may be changed
        path(scriptdir)


	output:
        //may need some changes here due to the multiple outputs in txt
		tuple val(suffix), path("uniq_contigs_over_third_quart_big_*.txt"), emit: slid_uniq_contig_ch
		path("*.txt")
		tuple val(suffix), path("sdr_over_third_quart_blocs_big_*.tsv"), emit: slid_uniq_tsv_ch
		path("*.tsv")
		path("slid_wind*.log")
		path("slid_wind*.cmd")

	script:
	"""
	slid_wind.sh ${coveragePerBin} ${suffix} ${obj} ${bin} ${percent} ${scriptdir} slid_wind.cmd >& slid_wind.log 2>&1
	"""
}