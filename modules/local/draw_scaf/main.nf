#!/usr/bin/env nextflow

/*
 * Draw the scaffolds
 */

process DRAW_SCAF{
	label 'midmem'

	tag "DRAW_SCAF"

	publishDir "${params.resultdir}/draw_scaf",	mode: 'copy', pattern: '*.png'
	publishDir "${params.resultdir}/logs/draw_scaf",	mode: 'copy', pattern: 'draw_scaf*.log'
	publishDir "${params.outdir}/00_pipeline_info/cmd",  mode: 'copy', pattern: 'draw_scaf*.cmd'

	input:
        tuple val(suffix), path(agpfile)
        path(blocfile)
        val(binsize)
        path(scriptdir)


	output:
		path("*.png")
		path("draw_scaf*.log")
		path("draw_scaf*.cmd")

	script:
	"""
	draw_scaf.sh ${agpfile} ${blocfile} ${binsize} ${scriptdir} draw_scaf.cmd >& draw_scaf.log 2>&1
	"""
}