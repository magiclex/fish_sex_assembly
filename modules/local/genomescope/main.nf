#!/usr/bin/env nextflow

/*
 * Make an histogram of kmers count
 */

process GENOMESCOPE{

    label 'lowmem'

    tag 'GENOMESCOPE'

    publishDir "${params.resultdir}/genomescope", mode: 'copy'
    publishDir "${params.resultdir}/logs/genomescope",	mode: 'copy', pattern: 'genomescope*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'genomescope*.cmd'


    input:
        tuple( path(histo), val(suffix) )
        val(kmersize)

    output:
        path outputDir, emit: genomescopeFolder
        path("genomescope*.log")
		path("genomescope*.cmd")

    script:
    """
    #module load statistics/R/4.2.2
    #module load bioinfo/GenomeScope2.0/eca7b88
    genomescope.sh ${histo} ${suffix} ${kmersize} genomescope.cmd >& genomescope.log 2>&1

    """

}