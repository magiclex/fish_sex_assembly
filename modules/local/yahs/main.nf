#!/usr/bin/env nextflow

/*
 * Check the content of the windows with R
 */

process YAHS{
	label 'midmem'

	tag "YAHS"

	publishDir "${params.resultdir}/yahs",	mode: 'copy' //whole directory to copy
	publishDir "${params.resultdir}/logs/yahs",	mode: 'copy', pattern: 'yahs*.log'
	publishDir "${params.outdir}/00_pipeline_info/cmd",  mode: 'copy', pattern: 'yahs*.cmd'

	input:
        tuple val(suffix), path(fai)
        tuple val(suffix2), path(selected_cont)
        tuple val(suffix3), path(sorted)
		// suffix, suffix2 and suffix3 should be the same
        val(species)
		val(enzymes)

	output:
        path outputDir, emit: yahs_folder
		tuples val(suffix), path("*_scaffolds_final.agp"), emit: yahs_agp_ch
		tuples val(suffix), path("*_scaffolds_final.fa"), emit: yahs_scaffold_ch
		path("yahs*.log")
		path("yahs*.cmd")

	script:
	"""
	yahs.sh ${fai} ${selected_cont} ${sorted} ${species} ${suffix} ${enzymes} yahs.cmd >& yahs.log 2>&1
	"""
}