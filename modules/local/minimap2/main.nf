#!/usr/bin/env nextflow

/*
 * Check the content of the windows with R
 */

process MINIMAP{
	label 'midmem'

	tag "MINIMAP"

	publishDir "${params.resultdir}/minimap2",	mode: 'copy', pattern: '*.sam'
	publishDir "${params.resultdir}/logs/minimap2",	mode: 'copy', pattern: 'minimap2*.log'
	publishDir "${params.outdir}/00_pipeline_info/cmd",  mode: 'copy', pattern: 'minimap2*.cmd'

	input:
        path(hic_r1_ch)
		path(hic_r2_ch)
        tuple val(suffix), path(selected_cont)
        val(species)

	output:
		tuple val(suffix), path("*.sam"), emit: minimap2_sam_ch
		path("minimap2*.log")
		path("minimap2*.cmd")

	script:
	"""
	minimap2.sh ${hic_r1_ch} ${hic_r2_ch} ${selected_cont} ${species} ${suffix} minimap2.cmd >& minimap2.log 2>&1
	"""
}