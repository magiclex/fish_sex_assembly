#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##                      Step 05-03: Transform bam to bed file                   ##
##                                                                          	##
##################################################################################

# Get script arguments coming from modules/local/bedtools/first/main.nf process
args=("$@")
BAM=${args[0]}
LOGCMD=${args[1]}

#Create new variables to name the different outputs
BEGINNING=$(echo "$SAM" | awk -F '.' '{OFS="."; $NF=""; print $0}' | sed 's/\.$//') 
BED=${BEGINNING}".bed"

# Commands to execute
#Conversion bam to bed
#CMD1="bedtools bamtobed -i ${OUTDIR}${SPECIES}_${STEP}_hapall_male_sorted.bam > ${OUTDIR}${SPECIES}_${STEP}_hapall_male_sorted.bed"
CMD="bedtools bamtobed -i ${BAM} > ${BED}"

# Save commands in log
echo ${CMD} > ${LOGCMD}

# Execution
eval ${CMD}

