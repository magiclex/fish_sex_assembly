#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##                          Step 07-01: Get the contigs                         ##
##                                                                          	##
##################################################################################

# Get script arguments coming from modules/local/getcontigs/main.nf process
args=("$@")
SELECTED=${args[0]}
HAPALLCONTIGS=${args[1]}
SPECIES=${args[2]}
SUFFIX=${args[3]}
SCRIPTDIR=${args[4]}
LOGCMD=${args[5]}

# New variables
NEWHAPALL=${SPECIES}"_"${SUFFIX}"_contigs_selected.fa"

# Commands to execute
#Make the counts
CMD="python3 ${SCRIPTDIR}get_contig.py \
	-i ${SELECTED} \
	-a ${HAPALLCONTIGS} \
	-o ${NEWHAPALL}
"

# Save commands in log
echo ${CMD} > ${LOGCMD}


# Execution
eval ${CMD}
