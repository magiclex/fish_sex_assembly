#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##			    Step 00-02: Make an histogram of the kmers			        	##
##                                                                          	##
##################################################################################

# Get script arguments coming from modules/local/jellyfish/histo/main.nf process
args=("$@")
COUNT=${args[0]}
SUFFIX=${args[1]}
NCPUS=${args[2]}
LOGCMD=${args[3]}

# Commands to execute
#CMD="jellyfish histo -t 16 ${OUTDIR}reads_male.jf > ${OUTDIR}reads_male.histo" #need modifications
CMD="jellyfish histo -t 16 ${COUNT} > reads_${SUFFIX}.histo"

# Save commands in log
echo ${CMD} > ${LOGCMD}

# Execution
eval ${CMD}
