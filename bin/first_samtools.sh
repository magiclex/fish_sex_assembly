#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##           Step 05-02: Check the mapping with samtools after bwamem           ##
##                                                                          	##
##################################################################################

# Get script arguments coming from modules/local/samtools/first/main.nf process
args=("$@")
SAM=${args[0]}
LOGCMD=${args[1]}

#Create new variables to name the different outputs
BEGINNING=$(echo "$SAM" | awk -F '.' '{OFS="."; $NF=""; print $0}' | sed 's/\.$//') 
BAM=${BEGINNING}".bam"
SORTED=${BEGINNING}"_sorted.bam"
COVERAGE=${BEGINNING}"_coverage.tsv"
GENOME=${BEGINNING}"_genome.txt"

# Commands to execute
#Conversion sam to bam
#CMD1="samtools view -@ 31 -bS ${SPECIES}_${STEP}_hapall_${SUFFIX}.sam > ${SPECIES}_${STEP}_hapall_${SUFFIX}.bam"
CMD1="samtools view -@ 31 -bS ${SAM} > ${BAM}"

#Sort
#CMD2="samtools sort -@ 31 ${BAM} > ${SPECIES}_${STEP}_hapall_${SUFFIX}_sorted.bam"
CMD2="samtools sort -@ 31 ${BAM} > ${SORTED}"

#Get the coverage
#CMD3="samtools coverage -H -o ${SPECIES}_${STEP}_hapall_${SUFFIX}_coverage.tsv ${SPECIES}_${STEP}_hapall_${SUFFIX}_sorted.bam"
CMD3="samtools coverage -H -o ${SORTED} ${BAM}"

#create genome
#CMD4="samtools view -H ${SPECIES}_${STEP}_hapall_${SUFFIX}_sorted.bam | grep '@SQ' | awk '{print substr($2,4) "\t" substr($3,4)}' > ${SPECIES}_${STEP}_hapall_${SUFFIX}_genome.txt"
CMD4="samtools view -H ${BAM} | grep '@SQ' | awk '{print substr(\$2,4) "\t" substr(\$3,4)}' > ${GENOME}"
#Check the min and max size of the contigs
#CMD5="samtools view -H ${SPECIES}_${STEP}_hapall_${SUFFIX}_sorted.bam | awk '$1 == "@SQ" {print $3}' | sed 's/LN://' | sort -n | awk 'NR==1{min=$1} {max=$1} END {print "Min contig size:", min; print "Max contig size:", max}'"
CMD5="samtools view -H ${BAM} | awk '\$1 == "@SQ" {print \$3}' | sed 's/LN://' | sort -n | awk 'NR==1{min=\$1} {max=\$1} END {print \"Min contig size:\", min; print \"Max contig size:\", max}'"

# Save commands in log
echo ${CMD1} > ${LOGCMD}
echo ${CMD2} >> ${LOGCMD}
echo ${CMD3} >> ${LOGCMD}
echo ${CMD4} >> ${LOGCMD}
echo ${CMD5} >> ${LOGCMD}

# Execution
eval ${CMD1}
eval ${CMD2}
eval ${CMD3}
eval ${CMD4}
eval ${CMD5}
