#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##                      Step 08-01: Draw the scaffolds                          ##
##                                                                          	##
##################################################################################

# Get script arguments coming from modules/local/draw_scaf/main.nf process
args=("$@")
AGP_FILE=${args[0]}
BLOC_FILE=${args[1]}
BIN_SIZE=${args[2]}
SCRIPTDIR=${args[3]}
LOGCMD=${args[4]}


# Commands to execute
#Draw the scaffolds
CMD="python3 ${SCRIPTDIR}draw_scaffolds.py \
	-1 ${AGP_FILE} \
	-2 ${BLOC_FILE} \
	-b ${BIN_SIZE} \
	-o ./
"

# Save commands in log
echo ${CMD} > ${LOGCMD}


# Execution
eval ${CMD}