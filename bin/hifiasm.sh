#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##			      Step 03-01: Assembly of Long Reads with Hi-C				    ##
##                                                                          	##
##################################################################################

# Get script arguments coming from modules/local/hifiasm/main.nf process
args=("$@")
SPECIES=${args[0]}
HIC1=${args[1]}
HIC2=${args[2]}
LONGREAD1=${args[3]}
LONGREAD2=${args[4]}
LOGCMD=${args[5]}
OUT=${SPECIES}"_assembly.asm.hic"
DATA="./longreads.fasta"

### Commands to execute

## Concatenation of the long reads
CMD1="cat ${LONGREAD1} ${LONGREAD2} > ${DATA}"

## Hifiasm
CMD2="hifiasm -o ${OUT} \
	--h1 ${HIC1} \
	--h2 ${HIC2} \
	-t 32 \
	${DATA}
"

## Reformat to fasta
# Contigs 1
CMD3="awk '/^S/{print \">\"\$2;print \$3}' ${OUT}.hic.hap1.p_ctg.gfa > ${OUT}.hap1.p_ctg.fa"

# Contigs 2
CMD4="awk '/^S/{print \">\"\$2;print \$3}' ${OUT}.hic.hap2.p_ctg.gfa > ${OUT}.hap2.p_ctg.fa"

## Concatenate the contigs
CMD5="cat ${OUT}.hap1.p_ctg.fa <(echo) ${OUT}.hap2.p_ctg.fa >> ${SPECIES}_hapall.fa"


### Save commands in log
echo ${CMD1} > ${LOGCMD}
echo ${CMD2} >> ${LOGCMD}
echo ${CMD3} >> ${LOGCMD}
echo ${CMD4} >> ${LOGCMD}
echo ${CMD5} >> ${LOGCMD}


### Execution

eval ${CMD1}
eval ${CMD2}
eval ${CMD3}
eval ${CMD4}
eval ${CMD5}