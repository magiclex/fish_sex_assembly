#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##                 Step 07-02: Map the HiC data on the contigs                  ##
##                                                                          	##
##################################################################################

# Get script arguments coming from modules/local/minimap2/main.nf process
args=("$@")
HIC1=${args[0]}
HIC2=${args[1]}
HAPALL=${args[2]}
SPECIES=${args[3]}
SUFFIX=${args[4]}
LOGCMD=${args[5]}


# New variable
FULLHIC="fusion_hic.fastq.gz"

### Commands to execute

# Concatenate the HiC files
CMD1="cat ${HIC1} ${HIC2} > ${FULLHIC}"

# Map the HiC on the contigs
CMD2="minimap2 \
        -x sr \
        -t 31 \
        -a \
        ${HAPALL} ${FULLHIC} > ${SPECIES}_${SUFFIX}_map_hic_hapall.sam
"

### Save commands in log
echo ${CMD1} > ${LOGCMD}
echo ${CMD2} >> ${LOGCMD}


### Execution
eval ${CMD1}
eval ${CMD2}