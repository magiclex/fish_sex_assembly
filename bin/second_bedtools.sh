#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##                      Step 06-01: Creating windows to count                   ##
##                                                                          	##
##################################################################################

# Get script arguments coming from modules/local/bedtools/second/main.nf process
args=("$@")
BED=${args[0]}
GENOME=${args[1]}
LOGCMD=${args[2]}

### Create new variables to name the different outputs  #TODO: check if correct
BEGGEN=$(echo "$GENOME" | awk -F '.' '{OFS="."; $NF=""; print $0}' | sed 's/\.$//') 
WINDOWED=${BEGGEN}"_bins.bed"
BEGBED=$(echo "$BED" | awk -F '.' '{OFS="."; $NF=""; print $0}' | sed 's/\.$//') 
OUT=${BEGBED}"_coveragePerBin.bed"

### Commands to execute
# Create the windows
#CMD1="bedtools makewindows -g ${INDIR}${SPECIES}_${PREVIOUS}_hapall_male_genome.txt -w ${BINS} > ${OUTDIR}${SPECIES}_${STEP}_bins_hapall_male.bed"
CMD1="bedtools makewindows -g ${GENOME} -w ${BINS} > ${WINDOWED}"
# Get the coverage per window
#CMD2="bedtools coverage -a ${OUTDIR}${SPECIES}_${STEP}_bins_hapall_male.bed -b ${INDIR}${SPECIES}_${PREVIOUS}_hapall_male_sorted.bed > ${OUTDIR}${SPECIES}_${STEP}_coverage_per_bin_hapall_male.bed"
CMD2="bedtools coverage -a ${WINDOWED} -b ${BED} > ${OUT}"

### Save commands in log
echo ${CMD1} > ${LOGCMD}
echo ${CMD2} >> ${LOGCMD}
### Execution
eval ${CMD1}
eval ${CMD2}
