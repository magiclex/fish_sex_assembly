#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##		               Step 01-01: Create a File of File                        ##
##                                                                          	##
##################################################################################

# Get script arguments coming from modules/local/FileOfFile/main.nf process
args=("$@")
MALE_FILES=${args[0]}
FEMALE_FILES=${args[1]}
SPECIES=${args[2]}
LOGCMD=${args[3]}

echo ${MALE_FILES}
echo ${FEMALE_FILES}

# New variables
FOF=${SPECIES}"_fof.txt"
MALE_FILES_STR=""
FEMALE_FILES_STR=""

# Commands to execute
# Convert the lists of files in strings for male
CMD1="
for file in \"${MALE_FILES[@]}\"; do
    MALE_FILES_STR+=\"$file ; \"
done
"


# Remove the last " ; "
CMD2="MALE_FILES_STR=${MALE_FILES_STR::-3}"
#Same for female
CMD3="
for file in \"${FEMALE_FILES[@]}\"; do
    FEMALE_FILES_STR+=\"$file ; \"
done
"
CMD4="FEMALE_FILES_STR=${FEMALE_FILES_STR::-3}"

# Write the file of file #MAYBE ERROR SOMEWHERE HERE WITH ESCAPE
CMD5="
awk -v male_files=\"$MALE_FILES_STR\" -v female_files=\"$FEMALE_FILES_STR\" -v out=${FOF} '
BEGIN {
    print \"MALES: \" male_files > out;
    print \"FEMALES: \" female_files >> out
}
'
"

for file in ${MALE_FILES}; do
    MALE_FILES_STR+="$file ; "
done
MALE_FILES_STR=${MALE_FILES_STR::-3}
for file in ${FEMALE_FILES}; do
    FEMALE_FILES_STR+="$file ; "
done
FEMALE_FILES_STR=${FEMALE_FILES_STR::-3}
awk -v male_files="$MALE_FILES_STR" -v female_files="$FEMALE_FILES_STR" -v out=${FOF} '
BEGIN {
    print "MALES: " male_files > out;
    print "FEMALES: " female_files >> out
}
'


# Save commands in log
echo ${CMD1} > ${LOGCMD}
echo ${CMD2} >> ${LOGCMD}
echo ${CMD3} >> ${LOGCMD}
echo ${CMD4} >> ${LOGCMD}
echo ${CMD5} >> ${LOGCMD}

# Execution
#eval ${CMD1}
#eval ${CMD2}
#eval ${CMD3}
#eval ${CMD4}
#eval ${CMD5}