#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##			      Step 02-01: Separation of kmtricks results				    ##
##                                                                          	##
##################################################################################


# Get script arguments coming from modules/local/selectKmers/main.nf process
args=("$@")
SPECIES=${args[0]}
SPEC=${args[1]}
LIMIT=${args[2]}
NCPUS=${args[3]}
LOGCMD=${args[4]}
OUT1=${SPECIES}"_02_kmunclean_female.txt"
OUT2=${SPECIES}"_02_kmunclean_male.txt"

# Commands to execute
CMD1="awk -v out1=${OUT1} -v out2=${OUT2} -v lim=${LIMIT} '{ if (\$2 == 0 && \$3 >= 10) { print \$0 >> out1} else if (\$3 == 0 && \$2 >= 10) { print \$0 >> out2} }' ${SPEC}"
### Transform to fasta
CMD2="awk 'BEGIN{count=0} {print \">kmer \" ++count \" male: \" \$2 \" \" \$3\"\n\"\$1;} ' ${SPECIES}_02_kmunclean_male.txt >> ${SPECIES}_02_kmunclean_male.fasta"

# Female
CMD3="awk 'BEGIN{count=0} {print \">kmer \" ++count \" female: \" \$2 \" \" \$3\"\n\"\$1;}' ${SPECIES}_02_kmunclean_female.txt >> ${SPECIES}_02_kmunclean_female.fasta"


# Save commands in log
echo ${CMD1} > ${LOGCMD}
echo ${CMD2} >> ${LOGCMD}
echo ${CMD3} >> ${LOGCMD}

# Execution
#eval ${CMD}
#awk -v out1=${OUT1} -v out2=${OUT2} -v lim=${LIMIT} '{ if ($2 == 0 && $3 >= 10) { print $0 >> out1} else if ($3 == 0 && $2 >= 10) { print $0 >> out2} }' ${SPEC}
#awk 'BEGIN{count=0} {print ">kmer " ++count " male: " $2 " " $3"\n"$1;} ' ${SPECIES}_02_kmunclean_male.txt >> ${SPECIES}_02_kmunclean_male.fasta
#awk 'BEGIN{count=0} {print ">kmer " ++count " female: " $2 " " $3"\n"$1;}' ${SPECIES}_02_kmunclean_female.txt >> ${SPECIES}_02_kmunclean_female.fasta
eval ${CMD1}
eval ${CMD2}
eval ${CMD3}