#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##           Step 09-01: Use to get the position of the kmers	                ##
##                                                                          	##
##################################################################################


# Get script arguments coming from modules/local/backToSequences/main.nf process
args=("$@")
SCAFFOLDS=${args[0]}
KMERS=${args[1]}
SPECIES=${args[2]}
SUFFIX=${args[3]}
KSIZE=${args[4]}
LOGCMD=${args[5]}

# New variables
OUT1="${SPECIES}_scaffolds_kmers_${SUFFIX}.txt"
OUT2="${SPECIES}_scaffolds_kmers_${SUFFIX}.fasta"


# Commands to execute
CMD1="back_to_sequences \
    --in-sequences ${SCAFFOLDS} \
    --in-kmers ${KMERS} \
    --out-kmers ${OUT1} \
    --kmer-size ${KSIZE} \
    --output-kmer-positions

"
## Number of kmers specific
CMD2="awk 'BEGIN{count=0} {if (NF > 1) ++count} END{print count}' ${OUT1}"
## Transform the kmers to fasta
CMD3="awk 'BEGIN{count=0} {if (NF > 1) {print \">kmer \" ++count \" ${SUFFIX} occuring on \" substr(\$0, index(\$0,\$2)) \"\n\" \$1;}}' ${OUT1} >> ${OUT2}"


# Save commands in log
echo ${CMD1} > ${LOGCMD}
echo ${CMD2} >> ${LOGCMD}
echo ${CMD3} >> ${LOGCMD}


# Execution
eval ${CMD1}
eval ${CMD2}
eval ${CMD3}
