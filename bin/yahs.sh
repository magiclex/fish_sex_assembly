#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##                Step 07-04: Assembly of the selected contigs                  ##
##                                                                          	##
##################################################################################

# Get script arguments coming from modules/local/yahs/main.nf process
args=("$@")
FAI=${args[0]}
NEWHAPALL=${args[1]}
SORTED=${args[2]}
SPECIES=${args[3]}
SUFFIX=${args[4]}
ENZYMES=${args[5]}
LOGCMD=${args[6]}

#NOTE: FAI is not used but should be present. FAI is the index of NEWHAPALL

# Commands to execute
CMD="yahs \
	${NEWHAPALL} \
	${SORTED} \
	-o ${SPECIES}_${SUFFIX} \
	-e ${ENZYMES}
"

# Save commands in log
echo ${CMD} > ${LOGCMD}

# Execution
eval ${CMD}

