#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##              Step 09-02: Draw the kmers on the scaffolds                     ##
##                                                                          	##
##################################################################################

# Get script arguments coming from modules/local/draw_curve/main.nf process
args=("$@")
KMERS=${args[0]}
AGP_FILE=${args[1]}
DRAW_BINS=${args[2]}
SCRIPTDIR=${args[3]}
LOGCMD=${args[4]}


# Commands to execute
#Draw the scaffolds
CMD="python3 ${SCRIPTDIR}draw_kmers_scaf.py \
        -k ${KMERS} \
        -s ${AGPFILE} \
        -o ./ \
	-b ${DRAW_BINS}

"

# Save commands in log
echo ${CMD} > ${LOGCMD}


# Execution
eval ${CMD}