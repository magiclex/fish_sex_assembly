#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##		            Step 00-03: Make an histogram                               ##
##                                                                          	##
##################################################################################

# Get script arguments coming from modules/local/genomescope/main.nf process
args=("$@")
HISTO_FILE=${args[0]}
OUTDIR=${args[1]}
KMER_SIZE=${args[2]}
LOGCMD=${args[3]}


# Commands to execute
CMD1="mkdir ${OUTDIR}"
CMD2="genomescope2 -i ${HISTO_FILE} -o ${OUTDIR} -k ${KMER_SIZE}"
#May need to check for things related to path; "Rscript" may also be needed
# Some issue with test because of it's small size: can't find heterozygous peak

# Save commands in log
echo ${CMD1} > ${LOGCMD}
echo ${CMD2} >> ${LOGCMD}

# Execution
eval ${CMD1}
eval ${CMD2}
