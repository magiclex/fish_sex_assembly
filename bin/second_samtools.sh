#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##                Step 07-03: Check the mapping after minimap2                  ##
##                                                                          	##
##################################################################################

# Get script arguments coming from modules/local/samtools/second/main.nf process
args=("$@")
SAM=${args[0]}
NEWHAPALL=${args[1]}
SPECIES=${args[2]}
SUFFIX=${args[3]}
LOGCMD=${args[4]}

# New variables
BAM=${SPECIES}"_"${SUFFIX}"_map_hic_hapall.bam"
SORTED=${SPECIES}"_"${SUFFIX}"_map_hic_hapall_sorted.bam"

# Commands to execute
# View and exclude unmapped
#CMD1="samtools view -bS -F 4 -@ 16 ${OUTDIR}${SPECIES}_${STEP}_map_hic_hapall.sam > ${OUTDIR}${SPECIES}_${STEP}_map_hic_hapall.bam"
CMD1="samtools view -bS -F 4 -@ 16 ${SAM} > ${BAM}"

#Sort
#CMD2="samtools sort -@ 16 -n ${OUTDIR}${SPECIES}_${STEP}_map_hic_hapall.bam > ${OUTDIR}${SPECIES}_${STEP}_map_hic_hapall_sorted.bam"
CMD2="samtools sort -@ 16 -n ${BAM} > ${SORTED}"

#Index
#CMD3="samtools faidx ${NEWHAPALL} > ${NEWHAPALL}.fai"
CMD3="samtools faidx ${NEWHAPALL} > ${NEWHAPALL}.fai"


# Save commands in log
echo ${CMD1} > ${LOGCMD}
echo ${CMD2} >> ${LOGCMD}
echo ${CMD3} >> ${LOGCMD}


# Execution
eval ${CMD1}
eval ${CMD2}
eval ${CMD3}
