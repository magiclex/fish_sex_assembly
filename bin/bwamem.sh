#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##           Step 05-01: Mapping the kmers on the selected contigs              ##
##                                                                          	##
##################################################################################

# Get script arguments coming from modules/local/bwamem2/main.nf process
args=("$@")
HAPALL=${args[0]}
KMERS=${args[1]}
KSIZE=${args[2]}
SUFFIX=${args[3]}
LOGCMD=${args[4]}

# need to find the suffix here
#male or female
#SUFFIX=$(echo "$KMERS" | awk '{size=split($0,NAMESEQ,"_")}; if (NAMESEQ[size]=="male.txt") {print "male"} else {print "female"}')
#ERROR should get also the number of the contigs (which brin)

# NOTE: reads files must be concatenated into one, which will be DATA
# Commands to execute
CMD1="bwa-mem2 index -p hapall ${HAPALL}"

CMD2="bwa-mem2 mem \
	-t 31 \
	-k ${KSIZE} \
	-T ${KSIZE} \
	-a \
	-c 500 \
	./hapall ${KMERS} > hapall_${SUFFIX}.sam
"

# Save commands in log
echo ${CMD1} > ${LOGCMD}
echo ${CMD2} >> ${LOGCMD}

# Execution
eval ${CMD1}
eval ${CMD2}