#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##                          Step 06-02: Making the counts                       ##
##                                                                          	##
##################################################################################

# Get script arguments coming from modules/local/slidwind/main.nf process
args=("$@")
COVERAGE=${args[0]}
SUFFIX=${args[1]}
OBJ=${args[2]}
BINS=${args[3]}
PERCENT=${args[4]}
SCRIPTDIR=${args[5]}
LOGCMD=${args[6]}

OUT="./windows_hapall_"${SUFFIX}".tsv"

# Commands to execute
#Make the counts
#CMD1="Rscript ${SCRIPTDIR}slid_wind_pipeline.R ${OUTDIR} ${OUTDIR}${SPECIES}_${STEP}_coverage_per_bin_hapall_female.bed ${OBJ} ${OUTDIR}${SPECIES}_${STEP}_hapall_female.tsv ${BINS} ${PERCENT}"
# TODO : check if it works to call the current directory like that for the script
CMD1="Rscript ${SCRIPTDIR}slid_wind_pipeline.R ./ ${COVERAGE} ${OBJ} ${OUT} ${BINS} ${PERCENT}"

# TODO: Modify the Rscript to incorporate the suffix in the name

#Get the name of the contigs
CMD2="awk '{print \$1}' ./sdr_over_third_quart_blocs_big.tsv | sort | uniq > ./uniq_contigs_over_third_quart_big_${SUFFIX}.txt"
CMD3="awk '{print \$1}' ./sdr_over_mean1std_blocs_big.tsv | sort | uniq > ./uniq_contigs_over_mean1std_big_${SUFFIX}.txt"
CMD4="awk '{print \$1}' ./sdr_over_mean2std_blocs_big.tsv | sort | uniq > ./uniq_contigs_over_mean2std_big_${SUFFIX}.txt"

CMD5="awk '{print \$1}' ./sdr_over_third_quart_blocs_small.tsv | sort | uniq > ./uniq_contigs_over_third_quart_small_${SUFFIX}.txt"
CMD6="awk '{print \$1}' ./sdr_over_mean1std_blocs_small.tsv | sort | uniq > ./uniq_contigs_over_mean1std_small_${SUFFIX}.txt"
CMD7="awk '{print \$1}' ./sdr_over_mean2std_blocs_small.tsv | sort | uniq > ./uniq_contigs_over_mean2std_small_${SUFFIX}.txt"

CMD8="awk '{print \$1}' ./sdr_over_third_quart_blocs_choice.tsv | sort | uniq > ./uniq_contigs_over_third_quart_choice_${SUFFIX}.txt"
CMD9="awk '{print \$1}' ./sdr_over_mean1std_blocs_choice.tsv | sort | uniq > ./uniq_contigs_over_mean1std_choice_${SUFFIX}.txt"
CMD10="awk '{print \$1}' ./sdr_over_mean2std_blocs_choice.tsv | sort | uniq > ./uniq_contigs_over_mean2std_choice_${SUFFIX}.txt"

# Save commands in log
echo ${CMD1} > ${LOGCMD}
echo ${CMD2} >> ${LOGCMD}
echo ${CMD3} >> ${LOGCMD}
echo ${CMD4} >> ${LOGCMD}
echo ${CMD5} >> ${LOGCMD}
echo ${CMD6} >> ${LOGCMD}
echo ${CMD7} >> ${LOGCMD}
echo ${CMD8} >> ${LOGCMD}
echo ${CMD9} >> ${LOGCMD}
echo ${CMD10} >> ${LOGCMD}

# Execution
eval ${CMD1}
eval ${CMD2}
eval ${CMD3}
eval ${CMD4}
eval ${CMD5}
eval ${CMD6}
eval ${CMD7}
eval ${CMD8}
eval ${CMD9}
eval ${CMD10}