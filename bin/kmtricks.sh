#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##           Step 01-02: Kmers Pool differenciation with kmtricks  	            ##
##                                                                          	##
##################################################################################

#module load devel/Miniconda/Miniconda3
#module load bioinfo/kmdiff/1.1.0


# Get script arguments coming from modules/local/kmtricks/main.nf process
args=("$@")
PLUGIN=${args[0]}
FOF=${args[1]}
WORKDIR=${args[2]}
SPECIES=${args[3]}
NCPUS=${args[4]}
LOGCMD=${args[5]}

EXCLUDIR=${WORKDIR}"/exclusive/"

# Commands to execute
##CMD="kmtricks pipeline --plugin ${PLUGIN} --file ${FOF} --run-dir /exclusive -t ${NCPUS};
##kmtricks aggregate --run-dir /exclusive/ --matrix kmer --format text --sorted > ${SPECIES}_specific_kmers.txt"

#CMD="kmtricksp pipeline --file ${FOF} --run-dir ${EXCLUDIR} -t ${NCPUS};
#kmtricks aggregate --run-dir ${EXCLUDIR} --matrix kmer --format text --sorted > ${SPECIES}_specific_kmers.txt"

CMD="kmtricksp pipeline --file ${FOF} --run-dir ./exclusive -t ${NCPUS};
kmtricks aggregate --run-dir ./exclusive --matrix kmer --format text --sorted > ${SPECIES}_specific_kmers.txt"

# Save commands in log
echo ${CMD} > ${LOGCMD}

# Execution
eval ${CMD}
