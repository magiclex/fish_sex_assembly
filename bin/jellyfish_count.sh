#!/usr/bin/bash
##################################################################################
##                                                                          	##
##			           Step 00-01: Count the kmers			        	        ##
##                                                                          	##
##################################################################################

# Get script arguments coming from modules/local/jellyfish/count/main.nf process
args=("$@")
DATA1=${args[0]}
DATA2=${args[1]}
SUFFIX=${args[2]}
NCPUS=${args[3]}
LOGCMD=${args[4]}

# Commands to execute
#CMD="jellyfish count -C -m 30 -s 1000000000 -t 16 <(zcat ${DATA1M}) <(zcat ${DATA2M}) -o reads_male.jf" #need modifications
CMD="jellyfish count -C -m 30 -s 1000000000 -t 16 <(zcat ${DATA1}) <(zcat ${DATA2}) -o reads_${SUFFIX}.jf"

# Save commands in log
echo ${CMD} > ${LOGCMD}

# Execution
eval ${CMD}

