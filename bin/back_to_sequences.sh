#!/usr/bin/env bash
##################################################################################
##                                                                          	##
##           Step 04-01: Select contigs with sex-specific kmers	                ##
##                                                                          	##
##################################################################################


# Get script arguments coming from modules/local/backToSequences/first/main.nf process
args=("$@")
SUFFIX=${args[0]}
HAPALL=${args[1]}
KMERS=${args[2]}
THRESHOLD=${args[3]}
KSIZE=${args[4]}
LOGCMD=${args[5]}

# need to find the suffix here
#male or female
#SUFFIX=$(echo "$KMERS" | awk '{size=split($0,NAMESEQ,"_")}; if (NAMESEQ[size]=="male.txt") {print "male"} else {print "female"}')

# New variables
OUT1="contig_hapall_"${SUFFIX}".fasta"
OUT3="names_contigs_hapall_"${SUFFIX}".txt"
OUT2="kmunclean_hapall_"${SUFFIX}".txt"
OUT4="km_no_null_hapall_"${SUFFIX}".fasta"

### Commands to execute
## Back_to_sequences
CMD1="back_to_sequences \
        --in-sequences ${HAPALL} \
        --in-kmers ${KMERS} \
        --out-sequences ${OUT1} \
        --out-kmers ${OUT2} \
        --kmer-size ${KSIZE} \
	    --output-kmer-positions \
        -m ${THRESHOLD}

"
## Counting the number of contigs and get their names
CMD2="awk 'BEGIN{count=0} {if (\$1 ~ />/ ) count = count + 1} END {print count}' ${OUT1}"

CMD3="awk '{if (\$1 ~ />/ ) print \$1}' ${OUT1} > ${OUT3}"

## Transform the kmers to fasta
CMD4="awk 'BEGIN{count=0} {if (NF > 1) ++count} END{print count}' ${OUT2}"
CMD5="awk 'BEGIN{count=0} \
    {if (NF > 1) \
    {print \">kmer \" ++count \" ${SUFFIX} occuring on \" substr(\$0, index(\$0,\$2)) \"\n\" \$1;}}'\
     ${OUT2} >> ${OUT4}"

### Save commands in log
echo ${CMD1} > ${LOGCMD}
echo ${CMD2} >> ${LOGCMD}
echo ${CMD3} >> ${LOGCMD}
echo ${CMD4} >> ${LOGCMD}
echo ${CMD5} >> ${LOGCMD}

### Execution
eval ${CMD1}
eval ${CMD2}
eval ${CMD3}
eval ${CMD4}
eval ${CMD5}