#! /usr/bin/env python3
import argparse

def length_scaffolds(agp_file):
	'''
        Function to get the size of the scaffolds
	to create rectangles.

        Parameters
        ----------
        agp_file: str
                The path of the file with the list of scaffolds.

        Returns
        -------
        Dict[str] = int
                A dictionnary with the name of the scaffolds as key,
        and their size as values.
        '''

	dict_scaf = {}
	with open(agp_file,'r') as agpf:
		for line in agpf:
			sline = line.strip()
			l_line = sline.split()
			name = l_line[0]
			stop = int(l_line[2])
			dict_scaf[name] = stop
	return dict_scaf

def get_blocs(bloc_file, bin_size):
	'''
        Function to get the list of positions of the blocs
	validated for each contig.

        Parameters
        ----------
        bloc_file: str
                The path of the file with the validate blocs.

	bin_size: int
		The size of the bins used to create blocs.

        Returns
        -------
        Dict[str] = List(Tuple(int,int))
		A dictionnary with the name of the contigs as key,
	and the list of tuples of the start and stop position of each bloc.
        '''

	dict_blocs = {}
	with open(bloc_file,'r') as bf:
		for line in bf:
			sline = line.strip()
			l_line = sline.split()
			if l_line[0] != "\"Contigs\"":
				name = l_line[0].strip("\"")
				start = int(l_line[2])
				end = int(l_line[3]) + int(bin_size) - 1
				if name in dict_blocs:
					l_pos = dict_blocs[name]
					l_pos.append((start,end))
					dict_blocs[name] = l_pos
				else:
					l_pos = list()
					l_pos.append((start,end))
					dict_blocs[name] = l_pos
	#print("List of blocs of contig of interest, h2tg000600l:")
	#print(dict_blocs["h2tg000600l"])
	return dict_blocs

def draw_result(agpfile, file2, bin_size, outdir):
	'''
	Function to draw the scaffolds with their contigs and
	the blocs validated on each contig.

	Parameters
	----------
	agpfile: str
		The path of the agp file.

	file2: str
		The path of the blocs file.

	bin_size: int
		The size of the bins used.

	outdir: str
		Path of the directory for the output.

	Returns
	-------
	None
	'''
	import matplotlib.pyplot as plt
	import matplotlib.patches as patches

	d_scaf = length_scaffolds(agpfile)
	d_blocs = get_blocs(file2,bin_size)
	d_patches = {}

	# Add the big rectangle representing each scaffold
	for scaffold in d_scaf:
		rect = patches.Rectangle((0, 0.25), d_scaf[scaffold], 
			0.5, linewidth=1, edgecolor='black', facecolor='gray', alpha=0.3)
		l_patches = list()
		l_patches.append((rect, " " , " ", " "))
		d_patches[scaffold] = l_patches

	# Add each bloc for each contig on the scaffold, and the intermediate parts
	with open(agpfile,'r') as agpf:
		for line in agpf:
			sline = line.strip()
			l_line = sline.split()
			name = l_line[0]
			start = int(l_line[1])
			stop = int(l_line[2])
			filling = l_line[4]
			l_patches = d_patches[name]
			if filling == "N":
				start_i = start
				inter = patches.Rectangle((start_i, 0.25), 200, 0.5, linewidth=1, edgecolor='black', facecolor='black', alpha=0.3)
				l_patches.append((inter,"filling", start_i, start_i + 200))
			else:
				contig = l_line[5]
				start_c = int(l_line[6])
				end_c = int(l_line[7])
				pos_1_c =  start
				pos_2_c =  start + (end_c - start_c)
				cont_rect = patches.Rectangle((start, 0.25), stop - start, 0.5, linewidth=1, edgecolor='black', facecolor='gray', alpha=0.1)
				l_patches.append((cont_rect, contig, start, stop))
				sign = l_line[8]
				count = 1
				if sign == "+":
					for (start_b,end_b) in d_blocs[contig]:
						if (start_b < start_c) and (end_b > start_c):
							true_start = start
							if end_b > end_c:	#case with bloc bigger than the part of contig
								true_end = stop
							else:
								true_end = start + (end_b - start_c) #case with bloc start before the start of the contig but end in the contig
							region_rect = patches.Rectangle((true_start, 0.25), true_end - true_start, 0.5, linewidth=1, edgecolor='red', facecolor='red', alpha=0.5)
							l_patches.append((region_rect, "bloc " + str(count), true_start, true_end))
						elif (start_b > start_c) and (start_b < end_c):
							true_start = start + (start_b - start_c)
							if end_b > end_c:		#case with start in the contig but end outside the contig
								true_end = stop
							else:
								true_end = start + (end_b - start_c)	#case with bloc entirely in the contig
							region_rect = patches.Rectangle((true_start, 0.25), true_end - true_start, 0.5, linewidth=1, edgecolor='red', facecolor='red', alpha=0.5)
							l_patches.append((region_rect, "bloc " + str(count), true_start, true_end))
							count += 1
				else:
					for (start_b,end_b) in d_blocs[contig]:
						if (end_b > end_c) and (start_b < end_c):
							true_start = start
							if start_b < start_c:	#case with bloc bigger than the part of contig
								true_end = stop
							else:
								true_end = start + (end_c - start_b)	#case with bloc start before the start of the contig but end in the contig
							region_rect = patches.Rectangle((true_start, 0.25), true_end - true_start, 0.5, linewidth=1, edgecolor='red', facecolor='red', alpha=0.5)
							l_patches.append((region_rect, "bloc " + str(count), true_start, true_end))
							count +=1
						elif (end_b < end_c) and (end_b > start_c):
							true_start = start + (end_c - end_b)
							if start_b < start_c:		#case with start in the contig but end outside the contig
								true_end = stop
							else:
								true_end = start + (end_c - start_b)	#ase with bloc entirely in the contig
							region_rect = patches.Rectangle((true_start, 0.25), true_end - true_start, 0.5, linewidth=1, edgecolor='red', facecolor='red', alpha=0.5)
							l_patches.append((region_rect, "bloc " + str(count), true_start, true_end))
							count += 1
			d_patches[name] = l_patches

	# Create the figure with the blocs
	for name in d_patches:
		fig, ax = plt.subplots(figsize=(20, 4))
		ax.set_xlim(0, d_scaf[name])
		ax.set_ylim(0, 1)
		ax.set_yticks([])
		ax.set_xticks([])
		l_patches = d_patches[name]
		for (rect, contig, start, end) in l_patches:
			ax.add_patch(rect)
			if (contig != " ") and (contig.startswith("bloc") == False) and (contig.startswith("filling") == False):
				ax.text((start + end) / 2, 0.1, contig, ha='center', va='center', fontsize=10, color='black', rotation=45)
			#if contig == "h2tg000600l":
				#print("Contig of interest found on " + str(name))
				#print("with bloc : " + str(start) + "-" + str(end))
			if name == "scaffold_38":
				print("Bloc of scaffold of interest: " + str(start) + "-" + str(end))
		ax.set_title(f"Scaffold {name} (Length: {d_scaf[name]})")
		plt.tight_layout()
		plt.savefig(f'{outdir}{name}.png', dpi=300)
		plt.close()

def main():
	'''
	Main Function to draw the scaffolds.

	Parameters
	----------
	None

	Returns
	-------
	None
	'''
	import numpy as np
	import matplotlib.pyplot as plt
	import matplotlib.patches as patches

	parser = argparse.ArgumentParser(description = "....")
	parser.add_argument("-1", help = "agp file of the scaffolds",
		dest = 'first', type = str, required = True)
	parser.add_argument("-2", help = "file of the blocs validated",
                dest = 'second', type = str, required = True)
	parser.add_argument("-b", help = "bin size",
		dest = 'bin_size', type = str, required = True)
	parser.add_argument("-o", help = "Outdir",
		dest = 'outdir', type = str, required = True)
	args = parser.parse_args()

	draw_result(args.first, args.second, int(args.bin_size), args.outdir)


if __name__ == "__main__":
	main ()
