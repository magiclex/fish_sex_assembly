#! /usr/bin/env python3
import argparse
import gzip

################################################################################
# This script use a list of selected contigs and a file of contigs
# to output a file ocntaining only the selected contigs.
# The names should be in a txt file, and the contigs in a fasta file.
# Given a correct path and name, the output will be generated there.
################################################################################


def parse_file(name_file):
	'''
	Function to parse a file containing the name of contigs,
	to return only the names in a list.

	Parameters
	----------
	name_file: str
		Path of a file with selected contigs (fasta format).

	Returns
	-------
	List(str)
		List of the names of the contigs.
	'''

	l_contigs = []
	with open(name_file,'r') as file_m:
		for line in file_m.readlines():
			sline = line.strip()
			if sline != "Contigs":
				contig= sline.strip('"')
				l_contigs.append(contig)
	return (l_contigs)

def get_correct_sequences(l_contigs,fasta_file):
	'''
	Function to get the correct sequences of the contigs,
	using a list of selected contigs and a file containing
	the sequence of all the contigs.

	Parameters
	----------
	l_contigs: List(str)
		List of contigs name.

	fasta_file: str
		Path to a fasta file containing the sequences of the contigs.

	Returns
	-------
	List(str)
		The list of the lines to write in a new file.

	'''

	writing = False
	lines_to_w = []
	with open(fasta_file, 'r') as file_a:
		for line in file_a:
			if line.startswith(">"):
				sline = line.strip()
				name = sline.strip(">")
				if name in l_contigs:
					writing = True
				else:
					writing = False
			if writing == True:
				lines_to_w.append(line)
	return(lines_to_w)

def main():
	'''
	Main Function to use a list of selected contigs and
	a file of sequences of contigs to output
	a file containing only the sequences of the selected contigs.
	The names should be in a txt file, and the contigs in a fasta file.
	Given a correct path and name, the output will be generated there.

	Parameters
	----------
	None

	Returns
	-------
	None
	'''


	parser = argparse.ArgumentParser(description = "Create a file with the selected contigs")
	parser.add_argument("-i", help = "File containing the names of the selected contigs",
		dest = 'inp', type = str, required = True)
	parser.add_argument("-a", help = "File containing the sequences of the contigs",
		dest = 'all', type = str, required = True)
	parser.add_argument("-o", help = "Output file name",
		dest = 'out', type = str, required = True)
	args = parser.parse_args()

	print("Searching the files...")
	names_file = str(args.inp)
	out_file = str(args.out)

	print("Get the contigs names...")
	l_contigs = parse_file(names_file)

	print("Get the sets...")
	lines_to_w = get_correct_sequences(l_contigs,args.all)

	print("Writing the correct contigs...")
	with open(out_file, 'wt') as file_m:
		for line in lines_to_w:
			file_m.writelines(line)

if __name__ == "__main__":
	main ()
