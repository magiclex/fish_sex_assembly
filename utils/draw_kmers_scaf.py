#! /usr/bin/env python3
import argparse
import gzip
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import re

def get_info_scaffold(scaf_file):
	'''
	Function to create a dictionnary with
	the number of the scaffold as key
	and the name + size of the scaffold as value.

	Parameters
	----------
	scaf_file: str
		The path of the agp file containing scaffolds.

	Returns
	-------
	Dict[int]: (str,int)
		The dictionnary of the scaffolds.
	'''
	count = -1
	d_scaf = {}
	name = ""
	with open(scaf_file,'r') as sf:
		for line in sf:
			sline = line.strip()
			l_line = sline.split()
			if name != l_line[0]:
				if name != "":
					d_scaf[str(count)] = (name, end)
					print("Scaffold number " + str(count) + " named " + str(name) + " and of size " + str (end))
				name = l_line[0]
				count += 1
				start = 1
				end = int(l_line[2])
			else:
				end = int(l_line[2])
		d_scaf[str(count)] = (name, end)
		print("Scaffold number " + str(count) + " named " + str(name) + " and of size " + str (end))
	return d_scaf


def bin_dataframe(d_scaf, bin_size):
	'''
	Function to create a data table using a file of scaffolds.
	The table is written in a new file.
	It represent a list of bins on the scaffold,
	with their number of sex-specific kmers ("Occurences").

	Parameters
	----------
	d_scaf: Dict[str]=(str,int)
		A dictionnary with the number of the scaffold as key
		and their name and size as value.

	bin_size: int
		The size of the bins to regroup the positions.

	Returns
	-------
	DataFrame
		A panda DataFrame with 3 columns:
		The number of the scaffold, the name of the bin,
		and the number of occurences in the bin (only 0 for now).
	'''
	d_lines = []
	for scaf in d_scaf:
		name = d_scaf[scaf][0]
		size = (d_scaf[scaf])[1]
		n_bins = int((size - (size % bin_size)) / bin_size)
		for i in range(n_bins + 1):
			bin = str(i * bin_size)
			new_line = {"Scaffold_number":str(name), "Bin":bin, "Occurences":0}
			d_lines.append(new_line)
	bin_df = pd.DataFrame(d_lines)
	print(bin_df.head())
	return bin_df

def populate_bin_table(kmer_file, outdir, bin_size, bin_df, d_scaf):
	'''
	Function to populate a data table using a file of kmers.
	The table is written in a new file.
	It represent a list of positions on the scaffolds.

	Parameters
	----------
	kmer_file: str
		The path of the file containing kmers.

	outdir: str
		The path of the directory to write the table into.

	bin_size: int
		The size of the bins to regroup the positions.

	bin_df: DataFrame
		The DataFrame to populate, with 3 columns:
		Number of the Scaffold, Bin, Occurences.

	Returns
	-------
	DataFrame
		A panda DataFrame with 3 columns:
		The number of the scaffold, the name of the bin,
		and the number of occurences in the bin.

	Other
	-------
	A tsv file of the DataFrame is created.

	'''
	d_previous = {}
	# Get the position of the matches
	with open(kmer_file,'r') as kf:
		for line in kf:
			sline = line.strip()
			l_line = sline.split()
			for match in l_line[1:]:
				l_match = (match.strip("()")).split(",")
				scaffold_n = str(l_match[0])
				scaffold = d_scaf[scaffold_n][0]
				position = int(l_match[1])
				bin = int(position - (position % bin_size))
				if scaffold in d_previous:
					if bin in d_previous[scaffold]:
						new = d_previous[scaffold][bin] + 1
						d_previous[scaffold][bin] = new
					else:
						d_previous[scaffold][bin] = 1
				else:
					d_previous[scaffold] = {}
					d_previous[scaffold][bin] = 1
	for scaffold in d_previous:
		for bin in d_previous[scaffold]:
			bin_df.loc[(bin_df['Scaffold_number'] == scaffold) & (bin_df['Bin'] == str(bin)), 'Occurences'] = d_previous[scaffold][bin]
	#bin_df.to_csv(path_or_buf = (outdir + "bin_df.tsv"), sep = "\t", index = False)
	return bin_df


def curv(bin_df, outdir):
	'''
	Function to create a curve of the number of kmers
	on each scaffold. The kmers are regrouped per bin.

	Parameters
	----------
	bin_df: DataFrame
		The DataFrame to populate, with 3 columns:
		Number of the Scaffold, Bin, Occurences.

	outdir: str
		The path of the output directory.

	Returns
	-------
	None

	Other
	-------
	A png file is created.

	'''
	# Check that the bins are in correct order
	bin_df['Bin_Number'] = bin_df['Bin'].str.extract('(\d+)').astype(int)
	# A New column to have each bin associated only to a scaffold,
	# and avoid having them together
	bin_df['Scaffold_Bin'] = bin_df['Scaffold_number'] + '_' + bin_df['Bin']


	bin_df.sort_values(by=['Scaffold_number', 'Bin_Number'], inplace=True)
	# Add a new column for the position, for putting on axis X
	bin_df['Position'] = range(len(bin_df))

	# Define alternating colors
	colors = ['blue', 'red']

	# Draw the curve
	plt.figure(figsize=(64, 12))
	#sns.lineplot(data=bin_df, x='Scaffold_Bin', y='Occurences', hue='Scaffold_number', marker='o')
	for i, scaffold in enumerate(bin_df['Scaffold_number'].unique()):
		scaffold_data = bin_df[bin_df['Scaffold_number'] == scaffold]
		plt.plot(scaffold_data['Position'], scaffold_data['Occurences'], 
			marker='o', color=colors[i % len(colors)], label=scaffold)

	# Add the scaffolds name on the X axis
	positions = bin_df['Position'].unique()
	scaffolds = bin_df['Scaffold_number'].unique()
	scaffold_positions = [bin_df[bin_df['Scaffold_number'] == scaffold]['Position'].mean() for scaffold in scaffolds]
	plt.xticks(scaffold_positions, scaffolds, rotation=90)
	plt.xlabel('Scaffold_number')

	plt.ylabel('Number of sex specific kmers (Occurences)')
	plt.title('Number of sex specific kmer per bin on each scaffold')
	plt.legend(title='Scaffold')
	plt.tight_layout()
	# Save it
	plt.savefig((outdir + "kmers_per_bin_per_scaffold_new.png"))
	bin_df.to_csv(path_or_buf = (outdir + "bin_df.tsv"), sep = "\t", index = False)

def main():
	'''
	Main Function to create a representation
	of the number of kmers sex-specific
	on scaffolds.

	Parameters
	----------
	None

	Returns
	-------
	None

	'''

	import numpy as np

	parser = argparse.ArgumentParser(description = "Create a data table with, for each kmer, its abundance and if it's in the SDR")
	parser.add_argument("-k", help = "Kmer file with abundance",
		dest = 'kmer', type = str, required = True)
	parser.add_argument("-s", help = "Scaffolds file",
		dest = 'scaf', type = str, required = True)
	parser.add_argument("-o", help = "Output directory",
		dest = 'out', type = str, required = True)
	parser.add_argument("-b", help = "Bin size",
		dest = 'bin_size', type = str, required = True)
	args = parser.parse_args()

	outdir = str(args.out)
	bin_size = int(args.bin_size)

	print("Get infos from the scaffolds")
	d_scaf = get_info_scaffold(args.scaf)
	print("Create a dataframe for the bins")
	bin_df = bin_dataframe(d_scaf, bin_size)
	print("Populate the dataframe")
	bin_df = populate_bin_table(args.kmer, outdir, bin_size, bin_df, d_scaf)
	print("Draw the curve")
	curv(bin_df, outdir)

if __name__ == "__main__":
	main ()
