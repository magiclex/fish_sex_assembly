# Fish_sex_assembly

## Introduction
The goal of this repository is to present a pipeline for the assembly of sexual chromosoms in fish species.
It is made with the supposition that you have short reads from PoolSeq separated between male and female, long reads from PacBio (sex not precised) and Hi-C data.
(For the environment, it is made to run on a cluster using slurm.)

## Description
The idea behind this project is to find and use specific kmers that should be present in only one sex, on the heterogametic chromosom. Since inbreeding can fix genes in both sex without sexual development implication, a region with continuity in the signal is looked for in the assembly in contigs. The contigs constituting this region are output in the end, with a new assembly in scaffold.

## Requirements
In order to be as FAIR as possible, this pipeline use containers to do each step. Some processes uses python and R, for which a singularity container definition file was made. This definition file should be used to create a new container localy, before running the pipeline, and its path should be changed if needed in the file `nextflow.config`. 

In the same way, a singularity container definition file was made for back_to_sequences and a new local container should be created.
Both definition files are in `./utils/containers`: `test_r_python2.def` (for R and python) and `test_b_to_s_sing.def` (for back_to_sequences).

The containers can be made using the following command: 
```{bash}
sudo singulariy build <container_name.sif> <container_def.sif>
```
 (it requires to have singularity installed)

To avoid issues and keeping it simple, the containers should be built in the folder of the containers (`./utils/containers`) with the same names. Else, the containers' name and path should be changed in `nextflow.config`.  
In case you have other available containers or want to create new ones on your own, the same changes should be applied.

This pipeline is a nextflow pipeline. Hence, netxflow should be installed, as should singularity for the containers.

## Installation
To use this pipeline, simply copying this repository with a 
```{bash} 
git clone
``` 
should be enough.

## DATA
This pipeline requires three type of data:
-short reads from Poolseq, the sequencing of two pools of individuals, separated between male and female
-long reads from the sequencing of one individual, supposed heterzygotic
-Hi-C data from the sequencing of the same individual

You should add your own data in the data folder and modify the `/conf/custom.config` file to use them
Some other input can be modified, parameters for the stringency or bin size, described below.

## Pipeline Description
10 steps with substeps constitute this pipeline.

The first one, step 0, gets info on the data to look at the rough composition in kmers. It should help check the quality of the sequencing, 
and tell if the kmer size chosen is a good choice. For this, `jellyfish` will count the kmers and `genomescope` will make an histogram with
the results.

Step 1 use kmtricks to find the kmers in each sex. To avoid errors their count have to be above a first threshold.

Step 2 create fasta files with the kmers present in only one of each sex (presumably more in the heterogametic sex, if not only in the heterogametic sex).

Step 3 make an assembly of the long reads in contigs. It is supposed that this assembly is good enough, and the result of the pipeline should point out the issues in case it is not.

Step 4 use back_to_sequences to find the selected kmers on the previous assembled contigs (but without their position).

In step 5, a mapping of this kmers is done to get their position.

Step 6 analyse the mapping, by cutting the contigs in bins and counting the number of sex specific kmers in each bin. 
Based on this number and if there is multiple bins contiguous, the contigs will be designed sex specific (or not).

Step 7 use those sex specific contigs to scaffold them. The SDR should be present in them.

Step 8 draw the scaffolds, the bins containing the sex-specific signal being represented.

Step 9 draw a curve of the number of kmers sex-specific per bin on the scaffold(s).

## Example
To run this pipeline, you should add your own data in the data folder and modify the `/conf/custom.config` file to use them.

Then use this command:
```{bash} 
nextflow run main.nf -profile custom,singularity
```

## Options


## TO DO 
Add a figure of the pipeline.

Make modification to add the possibility to use "module" instead of containers.

ifb-core.config >> passer en genotoul.config?

Add to the "Options" paragraph


## Authors and acknowledgment
This project was done after a 6-month internship at the LPGP INRAE (Rennes), under the supervision of Yann Guiguen, Christophe Klopp and Pierre Peterlongo.

It is also thanks to the authors of the various tools used in the script: back_to_sequences, bedtools, bwa-mem2, genomescope, hifiasm, jellyfish, kmtricks, minimap2, samtools and yahs.